import { useMemo } from "react";

export const useComputedFeedbackList = (memory, feedbackList) => {
    return useMemo(() => {
        return [
          ...feedbackList.filter(feedback => feedback.type === memory),
          ...feedbackList.filter(feedback => feedback.type !== memory),
        ]
      }, [memory, feedbackList]);
} 