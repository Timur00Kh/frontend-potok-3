import { createSlice } from "@reduxjs/toolkit";
const LOCAL_STORAGE_CART_KEY = 'LOCAL_STORAGE_CART_KEY';
const initialState = JSON.parse(localStorage.getItem(LOCAL_STORAGE_CART_KEY)) || [];

export const cartSlice = createSlice({
    name: "cart",
    initialState,
    reducers: {
        addProduct: (state, action) => {
            const isAlreadyInCart = state.findIndex(e => e === action.payload) > -1;

            if (isAlreadyInCart) {
                console.error('Такой товар уже есть в корзине!')
                return state;
            }

            const newState = [...state, action.payload];
            localStorage.setItem(LOCAL_STORAGE_CART_KEY, JSON.stringify(newState));
            return newState;
        },
        deleteProduct: (state, action) => {
            const newState = state.filter(id => id !== action.payload);
            localStorage.setItem(LOCAL_STORAGE_CART_KEY, JSON.stringify(newState));
            return newState;
        },
    }
});

export const { addProduct, deleteProduct } = cartSlice.actions;

export const cartReducer = cartSlice.reducer;