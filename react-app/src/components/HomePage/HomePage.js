import { Link } from "react-router-dom";
import { Footer } from "../Footer/Footer";
import { Header } from "../Header/Header";
import "./HomePage.css";


export function HomePage() {
  return (
    <div className="home-page">
      <Header />
      <div className="home-page__placeholder">
        <div className="home-page__placeholder-item">
          Здесь должно быть содержимое главной страницы. Но в рамках курса
          главная страница используется лишь в демонстрационных целях
        </div>
        <Link className="home-page__placeholder-link" to="/product">
          Перейти на страницу товара
        </Link>
      </div>
      <Footer />
    </div>
  );
}
