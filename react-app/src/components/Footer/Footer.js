import './Footer.css'

export function Footer() {

    return (
        <footer className="footer">
        <div className="wrapper footer__wrapper">
          <div className="footer__info">
            <div className="footer__copyright">
              <p className="footer__copyright-text">
                &#169; ООО «<span className="accent-text">Мой</span>Маркет»,
                2018-2022
              </p>
              <p>
                Для уточнения информации звоните по номеру
                <a className="link-primary" href="tel:79000000000">
                  +7 900 000 0000
                </a>
                ,
              </p>
              <p>
                а предложения по сотрудничеству отправляйте на почту
                <a className="link-primary" href="mailto:partner@mymarket.com">
                  partner@mymarket.com
                </a>
              </p>
            </div>
            <div className="footer__link">
              <a className="link-primary" href="#up">
                Наверх
              </a>
            </div>
          </div>
        </div>
      </footer>
    )
    
}