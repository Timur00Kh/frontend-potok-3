export function validateName(name) {
  if (name.length === 0) {
    return "Вы забыли указать имя и фамилию";
  }

  if (name.length < 2) {
    return "Имя не может быть короче 2-хсимволов";
  }
  return '';
}

export function validateRating(rating) {
  if (rating === 0 || isNaN(rating) || rating < 0 || rating > 5) {
    return false;
  }
  return true;
}
