import { useState, useEffect } from "react";
import {
  LOCAL_STORAGE_NAME_INPUT_KEY,
  LOCAL_STORAGE_RATING_INPUT_KEY,
  LOCAL_STORAGE_REVIEW_INPUT_KEY,
} from "../../../utils/constants";
import "./ReviewForm.css";
import { validateName, validateRating } from "./validate";

const errorModificatorClass = "error_active";
export function ReviewForm() {
  const [nameError, setNameError] = useState("");
  const [showRatingError, setShowRatingError] = useState(false);

  const [name, setName] = useState(
    localStorage.getItem(LOCAL_STORAGE_NAME_INPUT_KEY) || ''
  );
  const [rating, setRating] = useState(
    localStorage.getItem(LOCAL_STORAGE_RATING_INPUT_KEY) || ''
  );
  const [review, setReview] = useState(
    localStorage.getItem(LOCAL_STORAGE_REVIEW_INPUT_KEY) || ''
  );

  useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_NAME_INPUT_KEY, name);
    localStorage.setItem(LOCAL_STORAGE_RATING_INPUT_KEY, rating);
    localStorage.setItem(LOCAL_STORAGE_REVIEW_INPUT_KEY, review);
  }, [name, rating, review]);

  const onFormSubmit = (event) => {
    event.preventDefault();
    const nameError = validateName(name);
    const isRatingValid = validateRating(Number(rating));
    const isFormValid = !nameError && isRatingValid;

    setNameError(nameError);
    setShowRatingError(!isRatingValid);

    if (isFormValid) {
      localStorage.removeItem(LOCAL_STORAGE_NAME_INPUT_KEY);
      localStorage.removeItem(LOCAL_STORAGE_RATING_INPUT_KEY);
      localStorage.removeItem(LOCAL_STORAGE_REVIEW_INPUT_KEY);
      setName("");
      setRating("");
      setReview("");
    }
  };

  return (
    <form
      onSubmit={onFormSubmit}
      id="review-form"
      className="feedback-form"
      action="#"
      noValidate
    >
      <p className="feedback-form__title">Добавить свой отзыв</p>
      <div className="feedback-form__field">
        <div className="feedback-form__findings">
          <div className={"error " + (nameError ? errorModificatorClass : "")}>
            <input
              value={name}
              onChange={(e) => setName(e.currentTarget.value)}
              id="name"
              className="findings__username findings__input"
              type="text"
              name="username"
              placeholder="Имя и фамилия"
            />
            <div className="error__text">{nameError}</div>
          </div>
          <div
            className={
              "error " + (showRatingError ? errorModificatorClass : "")
            }
          >
            <input
              id="rating"
              className="findings__estimate findings__input"
              type="number"
              name="rating"
              min="1"
              max="5"
              step="1"
              placeholder="Оценка"
              value={rating}
              onChange={(e) => setRating(e.currentTarget.value)}
            />
            <div className="error__text">
              <div>Оценка должна быть от 1 до 5</div>
            </div>
          </div>
        </div>
        <div className="feedback-form__text">
          <textarea
            id="review"
            className="text__review findings__input"
            name="review"
            placeholder="Текст отзыва"
            value={review}
            onChange={(e) => setReview(e.currentTarget.value)}
          />
        </div>
        <div className="feedback-form__btn">
          <button className="btn btn_secondary" type="submit">
            Отправить отзыв
          </button>
        </div>
      </div>
    </form>
  );
}
