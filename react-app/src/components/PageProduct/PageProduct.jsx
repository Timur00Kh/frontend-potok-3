import "./PageProduct.css";
import { Header } from "../Header/Header";
import { FeedbackReview } from "./FeedbackReview/FeedbackReview";
import { ReviewForm } from "./ReviewForm/ReviewForm";
import { FavouriteButton } from "../FavouriteButton/FavouriteButton";
import React, { useCallback, useState } from "react";
import { feedbackList } from "../../utils/data";
import { Footer } from "../Footer/Footer";
import { useDispatch, useSelector } from "react-redux";
import { addProduct, deleteProduct } from "../../store/reducers/cart";
import { useComputedFeedbackList } from "../../utils/hook";
import { useEffect } from "react";

const currentProductId = 1;
export function PageProduct(params) {
  const [liked, setLiked] = useState(false);

  const dispatch = useDispatch();
  const addToCart = (id) => dispatch(addProduct(id));
  const removeFromCart = (id) => dispatch(deleteProduct(id));

  const cart = useSelector((store) => store.cart);
  const isCurrentProductInCart =
    cart.findIndex((id) => id === currentProductId) > -1;

  const onAddToCartBtnClick = useCallback(() => {
    if (isCurrentProductInCart) {
      removeFromCart(currentProductId);
    } else {
      addToCart(currentProductId);
    }
  }, [isCurrentProductInCart, currentProductId]);

  const [memory, setMemory] = useState("128ГБ");

  const computedFeedbackList = useComputedFeedbackList(memory, feedbackList);

  return (
    <div className="body-container">
      <Header />
      {/* <!--Контейнер для основного содержимого--> */}
      <div className="main-wrapper">
        {/* <!--Меню со ссылками--> */}
        <nav className="navigation">
          <a className="link-primary" href="./index.html">
            Электроника
          </a>
          <span>{">"}</span>
          <a className="link-primary" href="./index.html">
            Смартфоны и гаджеты
          </a>
          <span>{">"}</span>
          <a className="link-primary" href="./index.html">
            Мобильные телефоны
          </a>
          <span>{">"}</span>
          <a className="link-primary" href="./index.html">
            Apple
          </a>
        </nav>

        {/* <!--Основное содержимое сайта--> */}
        <main className="main">
          {/* <!--Контейнер с изображением товара--> */}
          <div className="product-image">
            <h2 className="product-image__title">
              Смартфон Apple iPhone 13, синий
            </h2>

            <div className="product-image__pic">
              <img
                className="product-image__pic-item"
                src="./resource/images/image-1.webp"
                alt="iPhone13 цвет: синий, вид экрана и задней панели"
                title="Вид экрана и задней панели"
              />
              <img
                className="product-image__pic-item"
                src="./resource/images/image-2.webp"
                alt="iPhone13 цвет: синий, вид экрана"
                title="Вид экрана"
              />
              <img
                className="product-image__pic-item"
                src="./resource/images/image-3.webp"
                alt="iPhone13 цвет: синий, вид экрана и задней панели сбоку"
                title="Вид экрана и задней панели сбоку"
              />
              <img
                className="product-image__pic-item"
                src="./resource/images/image-4.webp"
                alt="iPhone13 цвет: синий, камера"
                title="Вид камеры"
              />
              <img
                className="product-image__pic-item"
                src="./resource/images/image-5.webp"
                alt="iPhone13 цвет: синий, вид задней панели"
                title="Вид задней панели"
              />
            </div>
          </div>

          {/* <!--Контейнер с характеристиками товара и сайдбаром--> */}
          <div className="product-content">
            {/* <!--Контейнер с характиристиками товара--> */}
            <div className="product-content__params">
              {/* <!--Секция с цветом--> */}
              <section className="params-section">
                <h5 className="params-section__title">Цвет товара: Синий</h5>

                <div className="options">
                  {/* <!--Радио-кнопки выбора цвета товара--> */}
                  <label className="radio-btn">
                    <input
                      className="radio-btn__input visually-hidden"
                      type="radio"
                      name="color-select"
                      value="red"
                    />
                    <div className="radio-btn__elem radio-btn__elem-img">
                      <img
                        src="./resource/images/color-1.webp"
                        alt="iPhone13 цвет: красный"
                        title="Цвет: красный"
                        height="60"
                      />
                    </div>
                  </label>

                  <label className="radio-btn">
                    <input
                      className="radio-btn__input visually-hidden"
                      type="radio"
                      name="color-select"
                      value="khaki"
                    />
                    <div className="radio-btn__elem radio-btn__elem-img">
                      <img
                        src="./resource/images/color-2.webp"
                        alt="iPhone13 цвет: хаки"
                        title="Цвет: хаки"
                        height="60"
                      />
                    </div>
                  </label>

                  <label className="radio-btn">
                    <input
                      className="radio-btn__input visually-hidden"
                      type="radio"
                      name="color-select"
                      value="pink"
                    />
                    <div className="radio-btn__elem radio-btn__elem-img">
                      <img
                        src="./resource/images/color-3.webp"
                        alt="iPhone13 цвет: розовый"
                        title="Цвет: розовый"
                        height="60"
                      />
                    </div>
                  </label>

                  <label className="radio-btn">
                    <input
                      className="radio-btn__input visually-hidden"
                      type="radio"
                      name="color-select"
                      value="blue"
                    />
                    <div className="radio-btn__elem radio-btn__elem-img">
                      <img
                        src="./resource/images/color-4.webp"
                        alt="iPhone13 цвет: синий"
                        title="Цвет: синий"
                        height="60"
                      />
                    </div>
                  </label>

                  <label className="radio-btn">
                    <input
                      className="radio-btn__input visually-hidden"
                      type="radio"
                      name="color-select"
                      value="white"
                    />
                    <div className="radio-btn__elem radio-btn__elem-img">
                      <img
                        src="./resource/images/color-5.webp"
                        alt="iPhone13 цвет: белый"
                        title="Цвет: белый"
                        height="60"
                      />
                    </div>
                  </label>

                  <label className="radio-btn">
                    <input
                      className="radio-btn__input visually-hidden"
                      type="radio"
                      name="color-select"
                      value="black"
                    />
                    <div className="radio-btn__elem radio-btn__elem-img">
                      <img
                        src="./resource/images/color-6.webp"
                        alt="iPhone13 цвет: черный"
                        title="Цвет: черный"
                        height="60"
                      />
                    </div>
                  </label>
                </div>
              </section>

              {/* <!--Секция с конфигурациями памяти товара--> */}
              <section className="params-section">
                <h5 className="params-section__title">
                  Конфигурация памяти: 128 ГБ
                </h5>
                <div className="options">
                  {/* <!--Радио-кнопки выбора параметров памяти--> */}
                  {["128ГБ", "256ГБ", "512ГБ"].map((item) => (
                    <label key={item} className="radio-btn">
                      <input
                        className="radio-btn__input visually-hidden"
                        type="radio"
                        name="memory-select"
                        value={item}
                        checked={item === memory}
                        onChange={(e) =>
                          setMemory(e.currentTarget.checked ? item : "")
                        }
                      />
                      <div className="radio-btn__elem radio-btn__elem-btn">
                        {item}
                      </div>
                    </label>
                  ))}
                </div>
              </section>

              {/* <!--Секция характеристики товара--> */}
              <section className="params-section">
                <h5 className="params-section__title">Характеристики товара</h5>
                <ul className="params-section__list">
                  <li className="params-section__list-item">
                    <span>
                      Экран: <b>6.1</b>
                    </span>
                  </li>
                  <li className="params-section__list-item">
                    <span>
                      Встроенная память: <b>128 ГБ</b>
                    </span>
                  </li>
                  <li className="params-section__list-item">
                    <span>
                      Операционная система: <b>iOS 15</b>
                    </span>
                  </li>
                  <li className="params-section__list-item">
                    <span>
                      Беспроводные интерфейсы:
                      <b>NFC, Bluetooth, Wi-Fi</b>
                    </span>
                  </li>
                  <li className="params-section__list-item">
                    <span>
                      Процессор:
                      <b>
                        <a
                          className="link-primary"
                          href="https://ru.wikipedia.org/wiki/Apple_A15"
                          target="_blank"
                          rel="noreferrer"
                        >
                          Apple A15 Bionic
                        </a>
                      </b>
                    </span>
                  </li>
                  <li className="params-section__list-item">
                    <span>
                      Вес: <b>173 г</b>
                    </span>
                  </li>
                </ul>
              </section>

              {/* <!--Секция с описанием--> */}
              <section className="params-section">
                <h5 className="params-section__title">Описание</h5>
                <p className="params-section__text">
                  Наша самая совершенная система двух камер.
                  <br />
                  Особый взгляд на прочность дисплея.
                  <br />
                  Чип, с которым все супер быстро.
                  <br />
                  Аккумулятор держится заметно дольше.
                  <br />
                  <i>iPhone 13 - сильный мира сего.</i>
                </p>

                <p className="params-section__text">
                  Мы разработали совершенно новую схему расположения и
                  развернули объективы на 45 градусов. Благодаря этому внутри
                  корпуса поместилась наша лучшая система двух камер с
                  увеличенной матрицей широкоугольной камеры. Кроме того, мы
                  освободили место для системы оптической стабилизации
                  изображения сдвигом матрицы. И повысили скорость работы
                  матрицы на сверхширокоугольной камере.
                </p>

                <p className="params-section__text">
                  Новая сверхширокоугольная камера видит больше деталей в тёмных
                  областяхснимков. Новая широкоугольная камера улавливает на 47%
                  больше света для более качественных фотографий и видео. Новая
                  оптическая стабилизация со сдвигом матрицы обеспечит чёткие
                  кадры даже в неустойчивом положении.
                </p>

                <p className="params-section__text">
                  Режим «Киноэффект» автоматически добавляет великолепные
                  эффекты перемещения фокуса и изменения резкости. Просто
                  начните запись видео. Режим «Киноэффект»будет удерживать фокус
                  на объекте съемки, создавая красивый эффект размытиявокруг
                  него. Режим «Киноэффект» распознает, когда нужно перевести
                  фокус на другого человека или предмет, который появился в
                  кадре. Теперь ваши видео будут смотреться как настоящее кино.
                </p>
              </section>

              {/* <!--Секция с таблицей сравнение моделей--> */}
              <section className="params-section params-section_hidden">
                <h5 className="params-section__title">Сравнение моделей</h5>
                {/* <!--Таблица сравнения моделей--> */}
                <table className="params-section__table table">
                  <thead>
                    <tr className="table__head">
                      <th className="table__head-item">Модель</th>
                      <th className="table__head-item">Вес</th>
                      <th className="table__head-item">Высота</th>
                      <th className="table__head-item">Ширина</th>
                      <th className="table__head-item">Толщина</th>
                      <th className="table__head-item">Чип</th>
                      <th className="table__head-item">Объём памяти</th>
                      <th className="table__head-item">Аккумулятор</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr className="table__row">
                      <td className="table__cell-item">Iphone11</td>
                      <td className="table__cell-item">194 грамма</td>
                      <td className="table__cell-item">150,9 мм</td>
                      <td className="table__cell-item">75,7 мм</td>
                      <td className="table__cell-item table__cell-item_left">
                        8,3 мм
                      </td>
                      <td className="table__cell-item">A13 Bionic chip</td>
                      <td className="table__cell-item table__cell-item_left">
                        до 128 Гб
                      </td>
                      <td className="table__cell-item table__cell-item_left">
                        До 17 часов
                      </td>
                    </tr>
                    <tr className="table__row">
                      <td className="table__cell-item">Iphone12</td>
                      <td className="table__cell-item">164 грамма</td>
                      <td className="table__cell-item">146,7 мм</td>
                      <td className="table__cell-item">71,5 мм</td>
                      <td className="table__cell-item table__cell-item_left">
                        7,4 мм
                      </td>
                      <td className="table__cell-item">A14 Bionic chip</td>
                      <td className="table__cell-item table__cell-item_left">
                        до 256 Гб
                      </td>
                      <td className="table__cell-item table__cell-item_left">
                        До 19 часов
                      </td>
                    </tr>
                    <tr className="table__row">
                      <td className="table__cell-item">Iphone13</td>
                      <td className="table__cell-item">174 грамма</td>
                      <td className="table__cell-item">146,7 мм</td>
                      <td className="table__cell-item">71,5 мм</td>
                      <td className="table__cell-item table__cell-item_left">
                        7,65 мм
                      </td>
                      <td className="table__cell-item">A15 Bionic chip</td>
                      <td className="table__cell-item table__cell-item_left">
                        до 512 Гб
                      </td>
                      <td className="table__cell-item table__cell-item_left">
                        До 19 часов
                      </td>
                    </tr>
                  </tbody>
                </table>
              </section>
            </div>

            {/* <!--Сайдбар с ценой, кнопкой и рекламой--> */}
            <aside className="sidebar">
              {/* <!--Блок с ценой товара--> */}
              <div className="order-info">
                <div className="order-info__price">
                  <div className="order-info__price-value">
                    <div className="price__previous">
                      <div className="price__previous-value">75 990₽</div>
                      <div className="price__previous-discount">-8%</div>
                    </div>

                    <div className="price__current">67 990₽</div>
                  </div>
                  <FavouriteButton
                    liked={liked}
                    onClick={() => setLiked(!liked)}
                  />
                </div>
                {/* <!-- Блок с информацией по доставке--> */}
                <div className="order-info__delivery">
                  <p>
                    Самовывоз в четверг, 1 сентября — <b>бесплатно</b>
                  </p>
                  <p>
                    Курьером в четверг, 1 сентября — <b>бесплатно</b>
                  </p>
                </div>

                <button
                  onClick={onAddToCartBtnClick}
                  className={
                    "btn btn_primary btn_cart " +
                    (isCurrentProductInCart ? "btn_disabled " : "")
                  }
                >
                  {isCurrentProductInCart
                    ? "Товар уже в корзине"
                    : "Добавить в корзину"}
                </button>
              </div>
              {/* <!--Секция с рекламой--> */}
            </aside>
          </div>
        </main>

        {/* <!--Секция с отзывами--> */}
        <section className="feedback">
          <div className="feedback-header">
            <h3 className="feedback-header__title">Отзывы</h3>
            <span className="feedback-header__count">
              <b>425</b>
            </span>
          </div>

          <div className="feedback-review">
            {/* <!--Карточка с отзывом--> */}

            {computedFeedbackList.map((feedback, index) => (
              <React.Fragment key={feedback.author}>
                <FeedbackReview
                  author={feedback.author}
                  avatar={feedback.avatar}
                  rating={feedback.rating}
                  expirience={feedback.expirience}
                  advantage={feedback.advantage}
                  disadvantage={feedback.disadvantage}
                />
                {index < feedbackList.length - 1 ? (
                  <div className="separator"></div>
                ) : (
                  ""
                )}
              </React.Fragment>
            ))}
          </div>
        </section>

        {/* <!--Секция с формой отправки отзыва--> */}
        <section className="form">
          <ReviewForm />
        </section>
      </div>

      {/* <!--Подвал сайта--> */}
      <div style={{ marginTop: 100 }}>
        <Footer />
      </div>
    </div>
  );
}
