import "./FeedbackReview.css";

export function FeedbackReview({
  author,
  avatar,
  rating,
  expirience,
  advantage,
  disadvantage,
}) {
  const stars = new Array(5).fill();

  return (
    <div className="feedback-review__review-card">
      <img
        className="review-card__userphoto"
        src={avatar}
        alt="фото пользователя"
        title="Фотография пользователя"
      />
      <div className="review-card__content">
        <div className="review-card__content-header">
          <h4>{author}</h4>
          <div className="rating-result">
            {stars.map((el, index) => (
              <img
                key={index}
                className="rating-result__item"
                src={
                  index < rating
                    ? "./resource/images/Звезда.png"
                    : "./resource/images/Звезда серая.png"
                }
                alt="звезда жёлтая"
              />
            ))}
          </div>
        </div>

        <div className="review-card__content-info">
          <p>
            <b>Опыт использования:</b> {expirience}
          </p>
          <p>
            <b>Достоинства:</b>
            <br />
            {advantage}
          </p>

          <p>
            <b>Недостатки:</b>
            <br />
            {disadvantage}
          </p>
        </div>
      </div>
    </div>
  );
}
