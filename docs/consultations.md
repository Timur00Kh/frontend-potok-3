
# Записи консультаций

## Окрябрь
- [12.10.2022](https://events.webinar.ru/innopolisooc/879875815/record-new/2034159182) - Выполнили первые 3 домашки по JS, обсудили как отправлять ДЗ в робокод
- [23.10.2022](https://events.webinar.ru/innopolisooc/146441203/record-new/338069916) - Выполнили ДЗ с 20 по 23
- [26.10.2022](https://events.webinar.ru/innopolisooc/1969910478/record-new/656538335) - Выполнили ДЗ 24. Изучали DOM API, валидировали форму с помощью JS
- [30.10.2022](https://events.webinar.ru/innopolisooc/1580667801/record-new/623963399) - Выполнили ДЗ 25. Поработали с localStorage, сохранили данные формы. Еще раз разобрали промисы, разбирали вопросы и ошибки слушателей курса.

## Ноябрь

- [02.11.2022](https://events.webinar.ru/innopolisooc/Timur00Kh-potok-3/record-new/1963000113) - Выполнили ДЗ 26. Разобрали приватные поля классов и наследование.
- [06.11.2022](https://events.webinar.ru/innopolisooc/Timur00Kh-potok-3/record-new/1112001604) - разобрали промежуточную по JS, посмотреои как пользоваться дебагером и девтулзами хрома.
- [09.11.2022] (https://events.webinar.ru/innopolisooc/Timur00Kh-potok-3/record-new/1534378428) - инициализировали реакт приложение, разобрали структуру проекта
- [13.11.2022](https://events.webinar.ru/innopolisooc/Timur00Kh-potok-3/record-new/1432951834) - Перенесли верстку с прошлых модулей, разобрали компонентный подход
- [20.11.2022](https://events.webinar.ru/innopolisooc/Timur00Kh-potok-3/record-new/623608065) - Разобрали как делать логику формы на реакте. Svg в React, сделали кнопку добавления в избранное.
- [24.11.2022](https://events.webinar.ru/innopolisooc/1896867480/record-new/1086882298) - Сверстали Главную страницу, настроили роутинг.
- [29.11.2022](https://events.webinar.ru/innopolisooc/2082063950/record-new/1636293355) - Разобрались с редаксом. Обсудили иготовую аттестацию и презентацию. Разобрали критерии на разные оценки.

## Декабрь

- [02.12.2022](https://events.webinar.ru/innopolisooc/669442679/record-new/493950478) - рассмотрели хуки useMemo и useCallback
