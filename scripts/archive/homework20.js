// Упражнение 1
for (let i = 1; i <= 20; i++) {
    if (i % 2 === 0) {
        console.log(i);
    }
}

// Упражнение 1
let i = 1
while (i <= 20) {
    if (i % 2 === 0) {
        console.log(i);
    }
    i++;
}

// Упражнение 2
let sum = 0;
for (let i = 0; i < 3; i++) {
    const str = prompt('Введите число');
    const n = parseInt(str);
    if (isNaN(n) || str === null || str === '') {
        alert('Ошибка, выввели не число');
        break;
    }
    sum += n;
}
console.log(sum);

// Упражнение 3
function getNameOfMonth(n) {
    const months = [
        'Январь', 'Февраль', 'Март', 
        'Апрель', 'Май', 'Июнь', 'Июль', 
        'Август', 'Сентябрь', 'Октябрь', 
        'Ноябрь', 'Декабрь'
    ];
    
    if (n > months.length) {
        console.error('Невалидный индекс месяца');
        return;
    }

    return months[n];
}
console.log(getNameOfMonth(0));

for (let i = 0; i < 12; i++) {
    if (i === 9) continue;
    console.log(getNameOfMonth(i));
}
