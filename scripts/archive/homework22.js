// Упражнение 1
function getSumm(arr) {
    return arr
        .filter(e => typeof e === 'number')
        .reduce((sum, n) => sum + n, 0)
}

let arr1 = [1, 2, 10, 5];
console.log(getSumm(arr1)); // 18
let arr2 = ["a", {}, 3, 3, -2];
console.log(getSumm(arr2)); // 4

// Упражнение 3
// В корзине один товар
let cart = [4884];

// Добавили товар
addToCart(3456);
console.log(cart); // [4884, 3456]

// Повторно добавили товар
addToCart(3456);
console.log(cart); // [4884, 3456]

// Удалили товар
removeFromCart(4884);
console.log(cart); // [3456]

function addToCart(id) {
    const isAlreadyInCart = cart.findIndex(e => e === id) > -1;

    if (isAlreadyInCart) {
        console.error('Такой товар уже есть в корзине!')
        return;
    }

    cart.push(id);
}

function addToCartWithSet(id) {
    cart = Array.from(new Set([...cart, id]));
}

function removeFromCart(id) {
    cart = cart.filter(e => e !== id)
}


