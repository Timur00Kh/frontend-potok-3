"use strict";

// Упражнение 1.1
const secondsString = prompt("Введите кол-во секуннд для таймера", 10);
let seconds = parseInt(secondsString);

if (isNaN(seconds) || secondsString === null) {
  alert("Невалидный ввод секунд");
} else {
  const intervalId = setInterval(() => {
    if (seconds > 0) {
      seconds -= 1;
      console.log(`Осталось ${seconds}`);
    } else {
      console.log("Время вышло!");
      clearInterval(intervalId);
    }
  }, 1000);
}

// Упражнение 1.2 Promises
function timer(n) {
  const promises = [];

  for (let i = 0; i < n; i++) {
    promises.push(
      new Promise((resolve) =>
        setTimeout(() => {
          console.log(`Осталось ${n - i}`);
          resolve();
        }, i * 1000)
      )
    );
  }

  Promise.all(promises).then(() => {
    console.log("Время вышло!");
  });
}

// Упражнение 2
const requestTime = Date.now();
console.time("req");
fetch("https://reqres.in/api/users")
  .then((res) => res.json())
  .then((json) => {
    console.timeEnd("req");
    console.log(`Время исполнения запроса: ${Date.now() - requestTime}`);
    const { data: users } = json;
    console.log(
      users.reduce(
        (str, user) =>
          str + ` — ${user.first_name} ${user.last_name} (${user.email}) \n`,
        `Получили пользователей: ${users.length} \n`
      )
    );
  })
  .catch(() => console.error("Произожла ошибка"));

// Упражнение 2
(async function () {
  try {
    const response = await fetch("https://reqres.in/api/users");
    const json = await response.json();

    const { data: users } = json;
    console.log(
      users.reduce(
        (str, user) =>
          str + ` — ${user.first_name} ${user.last_name} (${user.email}) \n`,
        `Получили пользователей: ${users.length} \n`
      )
    );
  } catch (e) {
    console.error("Произожла ошибка");
  }
})();
