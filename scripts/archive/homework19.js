// Упражнение 1
let a = '$100'
let b = '300$'
let numberA = +a.slice(1);
let numberB = parseInt(b);

let summ = numberA + numberB; // Ваше решение
console.log(summ); // Должно быть 400

// Упражнение 2
let message = ' привет, медвед ';
message = message.trim();
message = message.charAt(0).toUpperCase() + message.slice(1) // Решение должно быть написано тут
console.log(message); // “Привет, медвед”

// Упражнение 3
const age = parseInt(prompt('Сколько вам лет?'));

if (age >= 0 && age <= 3) {
    alert(`Вам ${age} лет и вы младенец`)
} else if (age >= 4 && age <= 11) {
    alert(`Вам ${age} лет и вы ребенок`)
} else if (age >= 12 && age <= 18) {
    alert(`Вам ${age} лет и вы подросток`)
} else if (age >= 19 && age <= 40) {
    alert(`Вам ${age} лет и вы познаёте жизнь`)
} else if (age >= 41 && age <= 80) {
    alert(`Вам ${age} лет и вы познали жизнь`)
} else if (age >= 81) {
    alert(`Вам ${age} лет и вы долгожитель`)
}
console.log(age)

// 0 - 3 года — младенец
// 4 - 11 лет — ребенок
// 12 - 18 — подросток
// 19 - 40 — познаёте жизнь
// 41 - 80 — познали жизнь
// 81 и больше — долгожитель


// Упражнение 4
let str = 'Я работаю со строками как профессионал!';
let count = str.split(' ').length; // Ваше решение
console.log(count); // Должно быть 6
