// Упражнение 1

/**
 * Проверка объекта на наличие свойств
 * @param {object} obj - объект для проверки на наличие свойств
 * @return {boolean} результат проверки на наличие свойств
 */
function isEmpty(obj) {
    return Boolean(Object.keys(obj).length)
}

let user = {};
console.log(isEmpty(user)); // true
user.age = 12;
console.log(isEmpty(user)); // false

// Упражнение 3
let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};

/**
 * Увеличение ЗП каждому полю в объекете
 * @param {number} percent - процент в формате 0.05
 * @return {object} объект с повышенной ЗП
 */
function raiseSalary(percent) {
    const newSalaries = { ...salaries }; 

    for (const key in newSalaries) {
        newSalaries[key] *= 1 + percent
    }

    return newSalaries;
}

console.log(raiseSalary(0.05));


function raiseSalary2(percent) {
    return Object.entries(salaries)
        .reduce(
            (sum, [key, value]) => ({...sum, [key]: value * (1 + percent)}),
            {}
        )
}

console.log(raiseSalary2(0.05));

