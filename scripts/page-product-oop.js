
class Form {
    #inputs = [];
    #form = null;
    #LOCAL_STORAGE_INPUT_KEY;

    get inputs() {
        return this.#inputs;
    }

    get LOCAL_STORAGE_INPUT_KEY() {
        return this.#LOCAL_STORAGE_INPUT_KEY;
    }

    get form() {
        return this.#form;
    }


    constructor(form, name) {
        this.#form = form;
        this.#LOCAL_STORAGE_INPUT_KEY = `LOCAL_STORAGE_INPUT_KEY_${name}`

        this.#inputs = [
            ...form.querySelectorAll('input'),
            ...form.querySelectorAll('textarea'),
        ];

        const restoredData = JSON.parse(localStorage.getItem(this.LOCAL_STORAGE_INPUT_KEY)) 

        this.#inputs.forEach(
            (input) => {
                input.addEventListener('input', this.#saveInputData.bind(this));
                
                if (restoredData) {
                    const name = input.getAttribute('name');
                    input.value = restoredData[name] ? restoredData[name] : input.value;
                }
            }
        );

    }

    initOnSubmit() {
        this.#form.addEventListener('submit', this.#onSubmit.bind(this));
    }

    #saveInputData() {
        const saveData = this.#inputs.reduce((sum, input) => {
            const name = input.getAttribute('name');
            const value = input.value;
            return {
                ...sum,
                [name]: value
            }
        }, {});

        localStorage.setItem(this.#LOCAL_STORAGE_INPUT_KEY, JSON.stringify(saveData))
    }

    #onSubmit(event) {
        event.preventDefault();
        const isFormValid =  true;

        if (isFormValid) {
            this.#inputs.forEach(input => input.value = '');
            localStorage.removeItem(this.#LOCAL_STORAGE_INPUT_KEY);
        }
    }
}

class AddReviewForm extends Form {

    constructor(form, name, validateOnInput = false) {
        super(form, name)

        if ( validateOnInput){
            this.inputs.forEach(input => input.addEventListener('input', this.#validateReviewForm.bind(this)))
        }
    }

    initOnSubmit() {
        this.form.addEventListener('submit', this.#onSubmit.bind(this));
    }

    #onSubmit(event) {
        event.preventDefault();
        const isFormValid =  this.#validateReviewForm();

        if (isFormValid) {
            this.inputs.forEach(input => input.value = '');
            localStorage.removeItem(this.LOCAL_STORAGE_INPUT_KEY);
        }
    }

    #validateReviewForm() {
        const nameInput = this.form.querySelector("#name");
        const ratingInput = this.form.querySelector("#rating");


        const nameError = this.form.querySelector('#name-error');
        const ratingError = this.form.querySelector('#rating-error');

        const errorModificatorClass = 'error_active';

        const name = nameInput.value;
        const rating = +ratingInput.value;
      
        nameError.classList.remove(errorModificatorClass);
        ratingError.classList.remove(errorModificatorClass);
      
      
        if (name.length === 0) {
          nameError.querySelector('.error__text').innerText = "Вы забыли указать имя и фамилию";
          nameError.classList.add(errorModificatorClass)
          return false;
        }
      
        if (name.length < 2) {
          nameError.querySelector('.error__text').innerText = "Имя не может быть короче 2-хсимволов";
          nameError.classList.add(errorModificatorClass)
          return false;
        }
      
        if (rating === 0 || isNaN(rating) || rating < 0 || rating > 5) {
          ratingError.classList.add(errorModificatorClass);
          return false;
        }
        return true;
    }
}

const reviewFormController = new AddReviewForm(
        document.getElementById('review-form'), 'review-form',
    );
reviewFormController.initOnSubmit();




