'use strict';

const CURRENT_PRODUCT_ID = 1; // Мы должны были взять это значение из window.location
const LOCAL_STORAGE_CART_KEY = 'LOCAL_STORAGE_CART_KEY';
let CART = JSON.parse(localStorage.getItem(LOCAL_STORAGE_CART_KEY)) || [];

function addToCart(id) {
    const isAlreadyInCart = CART.findIndex(e => e === id) > -1;

    if (isAlreadyInCart) {
        console.error('Такой товар уже есть в корзине!')
        return;
    }

    CART.push(id);
    localStorage.setItem(LOCAL_STORAGE_CART_KEY, JSON.stringify(CART));
}

function removeFromCart(id) {
    CART = CART.filter(e => e !== id)
    localStorage.setItem(LOCAL_STORAGE_CART_KEY, JSON.stringify(CART));
}

/* DOM interaction */
const headerCartCounterDisabled = 'header__cart_counter-disabled';
function setCartCounter(cartLength) {
    const headerCart = document.getElementById('header-cart-button');
    headerCart.dataset.count = cartLength;

    if (cartLength <= 0) {
        headerCart.classList.add(headerCartCounterDisabled); 
    } else {
        headerCart.classList.remove(headerCartCounterDisabled); 
    }
}

const btnDisabledClass = 'btn_disabled';
function toggleCartButton(isDisabled) {
    const cartButton = document.querySelector('.btn_cart');
    
    if (typeof isDisabled !== 'boolean') {
        isDisabled = !cartButton.classList.contains(btnDisabledClass);
    }

    if (isDisabled) {
        cartButton.innerText = 'Товар уже в корзине';
        cartButton.classList.add(btnDisabledClass);
    } else {
        cartButton.innerText = 'Добавить в корзину';
        cartButton.classList.remove(btnDisabledClass);
    }
}

function onAddProductToCart(productId) {

    const isAlreadyInCart = CART.findIndex(e => e === productId) > -1;
    
    if (isAlreadyInCart) {
        removeFromCart(productId);
    } else {
        addToCart(productId);
    }

    toggleCartButton(!isAlreadyInCart);
    setCartCounter(CART.length);
}

/* Product Page Initialization */
document.querySelector('.btn_cart').addEventListener('click', () => onAddProductToCart(CURRENT_PRODUCT_ID));
toggleCartButton(CART.some(id => id === CURRENT_PRODUCT_ID));
setCartCounter(CART.length);


