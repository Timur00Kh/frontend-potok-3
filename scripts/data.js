
const product = {
    id: 1,
    title: 'iPhone',
    color: 'space red',
    memory: '256gb',
    fullPrice: 100000,
    purchasePrice: 80000,
    discount: 20
}

const feedback1 = {
    author: {
        name: 'Тимур',
        avatar: 'http://avatar.ru/timur.png'
    },
    text: 'Хороший айфон! Мне норм',
    rating: 4.6
}

const feedback2 = {
    author: {
        name: 'Timur',
        avatar: 'http://avatar.ru/timur.png'
    },
    text: 'Хороший айфон! Мне норм',
    rating: 3.7
}
