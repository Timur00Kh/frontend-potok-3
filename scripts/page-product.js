'use strict';

const reviewForm = document.getElementById('review-form');
const nameInput = document.getElementById("name");
const ratingInput = document.getElementById("rating");
const reviewTextarea = document.getElementById('review');

const nameError = document.getElementById('name-error');
const ratingError = document.getElementById('rating-error');



function validateReviewForm() {
  const name = nameInput.value;
  const rating = +ratingInput.value;

  nameError.classList.remove(errorModificatorClass);
  ratingError.classList.remove(errorModificatorClass);


  if (name.length === 0) {
    nameError.querySelector('.error__text').innerText = "Вы забыли указать имя и фамилию";
    nameError.classList.add(errorModificatorClass)
    return false;
  }

  if (name.length < 2) {
    nameError.querySelector('.error__text').innerText = "Имя не может быть короче 2-хсимволов";
    nameError.classList.add(errorModificatorClass)
    return false;
  }

  if (rating === 0 || isNaN(rating) || rating < 0 || rating > 5) {
    ratingError.classList.add(errorModificatorClass);
    return false;
  }
  return true;
}

reviewForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const isFormValid = validateReviewForm();
    if (isFormValid) {
        removeReviewInputData();
    }
})

function saveReviewInputData() {
    const name = nameInput.value;
    const rating = +ratingInput.value;
    const reviewText = reviewTextarea.value;

    localStorage.setItem(LOCAL_STORAGE_INPUT_KEY, JSON.stringify({
        name, rating, reviewText
    }))
}

function removeReviewInputData() {
    localStorage.removeItem(LOCAL_STORAGE_INPUT_KEY);
    nameInput.value = '';
    ratingInput.value = '';
    reviewTextarea.value = '';
}

[ratingInput, nameInput, reviewTextarea]
    .forEach(input => input.addEventListener('input', saveReviewInputData));

const reviewInputData = JSON.parse(localStorage.getItem(LOCAL_STORAGE_INPUT_KEY));

if (reviewInputData) {
    nameInput.value = reviewInputData.name;
    ratingInput.value = reviewInputData.rating;
    reviewTextarea.value = reviewInputData.reviewText;
}


